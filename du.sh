echo "Getting the disk usage"

sudo du -s /data/* > current_size.txt
df -h /data > total_usage.txt

echo "Making the graph"

Rscript make_graphs2.r

echo "Done"

echo "Writing previous_size.txt file"

#cp current_size.txt previous_size.txt

echo "All done!"
