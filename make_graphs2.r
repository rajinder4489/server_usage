library("reshape")
library("ggplot2")
library("gridExtra")
library("cowplot")

#dus <- read.delim(file = "size_all.txt", sep = "\t", stringsAsFactors = F)
total <- read.delim(file = "total_usage.txt", sep = "\t", stringsAsFactors = F)
total_string <- total[1,1]
x <- gsub("  ", " ", total_string)
x <- gsub("  ", " ", x)
x <- strsplit(x, " ")

dus_cur <- read.delim(file = "current_size.txt", sep = "\t", stringsAsFactors = F, header = F)

dus_cur$V2 <- sub("/data/", "", dus_cur$V2)
colnames(dus_cur) <- c("size", "Folders")

melted <- melt(dus_cur)

melted$value <- round(melted$value/(1024 * 1024 * 1024), digits = 4)		#converting to Tbs; values obtained are in Kbs

p1 <- ggplot(data=melted, aes(x= reorder(Folders, -value), y=value)) + 
	geom_bar(stat="identity") +
	coord_flip() +
	theme(axis.text.x = element_text(angle = 30, vjust = 0.5, hjust = 1), axis.text.y = element_text(vjust = 0.5, hjust = 1)) +
	labs(title = "Folder sizes on Server\nunder data folder", subtitle =Sys.Date(), x = "Folders", y = "size in Tbs", tag = "A") + 
	annotate("label", x = (nrow(melted)-2), y = (max(melted$value)-1), label = paste("Total = ", x[[1]][2], "\n Used =", x[[1]][3], "\nAvail = ", x[[1]][4]))


#calculating and plotting the change
dus_pre <- read.delim(file= "previous_size.txt", sep="\t", stringsAsFactors = F, header = F)
dus_pre$V2 <- sub("/data/", "", dus_pre$V2)
colnames(dus_pre) <- c("size", "Folders")


dff <-  merge(dus_cur, dus_pre,  by="Folders", all=T, suffixes = c("_current", "_previous"))
dff[is.na(dff)] <- 0

#calculate the change
dff$change <- dff$size_current - dff$size_previous

#If value of previous == 0, change it to the value of change calculated in previous step. Required to calculate % change 
dff[dff[,"size_previous"] == 0, "size_previous"] <- dff[dff[,"size_previous"] == 0, "change"]

#calculating %change
dff$change_percent <- dff$change/dff$size_previous * 100

#convert the values of change to Gbs
dff$change <- round(dff$change/(1024 * 1024), digits = 2)



subset_change <- dff[dff[,"change"] != 0 , c("Folders", "change")]
subset_percent <- dff[dff[,"change_percent"] != 0 , c("Folders", "change_percent")]

changed <- subset_change[order(subset_change$change, decreasing = T), "Folders"]
xx <- system("sudo ls -lh /data/", intern = TRUE)
xx_list <- strsplit(xx, " ")

names <- data.frame()
for(z in 1:length(xx))
{
	yy <- strsplit(xx[[z]], " ")
	for(folder in changed)
	{
		if(yy[[1]][length(yy[[1]])] == folder)
		{
			values <- c(yy[[1]][length(yy[[1]])], yy[[1]][4])
			names <- rbind(names, values)
		}
	}
}
colnames(names) <- c("Folders", "User")
subset_change = merge(subset_change, names, by="Folders")

#melted2 <- melt(subset_change)
p2 <- ggplot(data=subset_change, aes(x= reorder(Folders, -change), y=change, fill=Folders)) +
        geom_bar(stat="identity") +
	geom_text(aes(label=User), position=position_dodge(width=0.9), vjust=-0.25) +
#        theme(axis.text.x = element_text(angle = 60, vjust = 0.5, hjust = 1), axis.text.y = element_text(vjust = 0.5, hjust = 1), legend.position = "none") +
	theme(axis.text.x = element_blank(), axis.title.x=element_blank(), axis.text.y = element_text(vjust = 0.5, hjust = 1), legend.position = "right") +
	labs(title = "Change in size", x = "Folders", y = "size in Gbs", tag = "B")


#melted3 <- melt(subset_percent)
p3 <- ggplot(data=subset_percent, aes(x= reorder(Folders, -change_percent), y=change_percent, fill=Folders)) +
        geom_bar(stat="identity") +
#	geom_text(aes(label=value), position=position_dodge(width=0.9), vjust=-0.25) +		#to add values to the bars
#        theme(axis.text.x = element_text(angle = 60, vjust = 0.5, hjust = 1), axis.text.y = element_text(vjust = 0.5, hjust = 1), legend.position = "none") +
        theme(axis.text.x = element_blank(), axis.title.x=element_blank(), axis.text.y = element_text(vjust = 0.5, hjust = 1), legend.position = "right") +
	labs(title = "Percent change in size", x = "Folders", y = "% change", tag= "C")


#for_legend <- ggplot(data=melted3, aes(x= reorder(Folders, -value), y=value, fill=Folders)) +
#        geom_bar(stat="identity")
#legend <- get_legend(for_legend)


#saving individual graphs
#ggsave(paste("disk_usage_", Sys.Date(), ".jpeg", sep =""), p1)
#ggsave("test1.jpeg", p2)
#ggsave("test2.jpeg", p3)

#making the grid
#plot_grid <- grid.arrange(p1, p2, p3, legend, nrow = 2, layout_matrix = rbind(c(1, 1, 1), c(2, 4, 3)))

plot_grid <- grid.arrange(p1, p2, p3, nrow = 2, layout_matrix = rbind(c(1, 1), c(2, 3)))

#saving the plot grid
ggsave(paste("disk_usage_", Sys.Date(), ".jpeg", sep =""), plot_grid, width = 15, height = 15, units = "in") 
